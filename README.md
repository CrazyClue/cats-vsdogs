# catsVSdogs

#### 介绍
猫狗二分类图像识别


#### 安装教程

1.  在config.py中修改配置
2.  修改Torch.CPU/CUDA适配本机环境

#### 使用说明

main文件中只封装了模块，运行时会打印参数。
1.  python main.py train 进行模型训练
2.  python main.py val   进行验证
3.  python main.py test  进行测试 

